<?php
/**
 * The Header of our theme
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package darwin
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="image" content="screenshot.png">
        <meta name="description" content="<?php echo (get_bloginfo( 'description' ) ? get_bloginfo( 'description' ) : _e("Default canvas developed to make the Darwin Digital's websites creation easier", "darwin")); ?>">

        <!-- favicon cross-plateform / default favicon = favicon.ico -->
        <link rel="icon" href="<?php echo get_template_directory_uri() ."/images/favicon.ico" ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ."/images/favicon-32x32.png" ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ."/images/favicon-16x16.png" ?>">

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ."/images/apple-touch-icon.png" ?>">
        <link rel="manifest" href="<?php echo get_template_directory_uri() ."/images/site.webmanifest" ?>">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri() ."/images/safari-pinned-tab.svg" ?>" color="#143251">
        <meta name="msapplication-config" content="<?php echo get_template_directory_uri() . "/images/browserconfig.xml" ?>"/>
        <meta name="msapplication-TileColor" content="#ecf0f1">
        <meta name="theme-color" content="#ffffff">

        <title><?php !is_front_page() ? bloginfo('name') . wp_title('-') : bloginfo('name'); ?></title>
        <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css"/>
        <?php wp_head(); ?>
    </head>

    <body>
        <header>

        </header>