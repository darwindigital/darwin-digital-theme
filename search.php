<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package darwin
 */
?>

<header class="page-header">
    <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'darwin' ), get_search_query() ); ?></h1>
</header> <!-- .page-header -->
