<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package darwin
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
    <div class="page-wrapper error-404 not-found">
        <header class="page-header">
        <h1><?php _e( 'This is somewhat embarrassing, isn’t it?', 'darwin' ); ?></h1>
        </header>

        <section class="page-content">
            <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'darwin' ); ?></p>

            <?php get_search_form(); ?>
        </section><!-- .page-content -->
    </div><!-- .page-wrapper -->
</main> <!-- #main -->

<?php get_footer(); ?>