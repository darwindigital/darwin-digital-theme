<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package darwin
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

<?php
// Start the loop.
while ( have_posts() ) : 
    the_post();

    /*
        * Include the post format-specific template for the content. If you want to
        * use this in a child theme, then include a file called called content-___.php
        * (where ___ is the post format) and that will be used instead.
        */
    get_template_part( 'content', get_post_format() );

    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
        comments_template();
    endif;

    // Previous/next page navigation.
    the_posts_pagination(
        array(
            'prev_text'          => __( 'Previous page', 'darwin' ),
            'next_text'          => __( 'Next page', 'darwin' ),
            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'darwin' ) . ' </span>',
        )
    );

// End the loop.
endwhile;
?>

</main> <!-- #main -->

<?php get_footer(); ?>