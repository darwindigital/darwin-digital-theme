<?php
/**
 * Darwin Digital Theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package darwin
 */

if ( ! function_exists( 'darwin_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function darwin_setup() {
        /**
         * Make theme available for translation.
         * Translations can be placed in the /languages/ directory.
         */
        load_theme_textdomain( 'darwin', get_template_directory() . '/languages' );
    
        /**
         * Add default posts and comments RSS feed links to <head>.
         */
        add_theme_support( 'automatic-feed-links' );
    
        /**
         * Enable support for post thumbnails and featured images.
         */
        add_theme_support( 'post-thumbnails' );

        // Featured image size for posts and pages
        add_image_size('custom-size', 'width', 'height');
    
        /**
         * Add support for two custom navigation menus.
         */
        register_nav_menus( array(
            'primary'   => __( 'Primary Menu', 'darwin' ),
            'secondary' => __('Secondary Menu', 'darwin' )
        ) );
    
        /**
         * Enable support for the following post formats:
         * aside, gallery, quote, image, and video
         */
        add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
    } 
endif;
add_action( 'after_setup_theme', 'darwin_setup' );

function darwin_scripts() {
    wp_register_script( 'jQuery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js', null, null, true );
    wp_enqueue_script('jQuery');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap-grid.min-v4.3.1.css', array(), '4.3.1' );
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize-v8.0.1.css', array(), '8.0.1' );
}
add_action( 'wp_enqueue_scripts', 'darwin_scripts' );

function wpb_add_googleanalytics() {

    // Paste your Google Analytics tracking code here

}
add_action('wp_head', 'wpb_add_googleanalytics');

function dd_register_sidebars() {
    /* Register the 'primary' sidebar. */
    register_sidebar(
        array(
            'id'            => 'primary',
            'name'          => __( 'Primary Sidebar' ),
            'description'   => __( 'A short description of the sidebar.' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
    /* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'dd_register_sidebars' );

// Add Theme and Footer Settings page on backend (NEED ACF INSTALLED)
if ( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
    acf_add_options_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer Settings',
		'menu_slug' 	=> 'footer-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function acf_responsive($image_id,$image_size,$max_width){
	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';

	}
}

/* -----------------------------
 * BREADCRUMB NAV FUNCTION
 * ----------------------------- */
function darwin_breadcrumb() {
    $sep = '<img src="'.get_template_directory_uri().'/images/logos-and-icons/arrow-right.png" alt="Separator"></li>';
    $home_name = 'Home';
    $before_wrap = '<ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';
    $after_wrap = '</ul>';
    
    global $post;
    $home_url = get_bloginfo( 'url' );

    $breadcrumbs = "";    
	$breadcrumbs .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . $home_url . '">' . $home_name . '</a>';
    
    // Add News title if post page
    if (is_home()){
        $page_for_posts_id = get_option('page_for_posts');
        if ( $page_for_posts_id ) { 
            $post = get_page($page_for_posts_id);
            setup_postdata($post);
            $breadcrumbs .= $sep . '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">' . get_the_title( $post ) . '</span></li>';
            rewind_posts();
        }
	}
	
	if( is_post_type_archive( 'recipes' ) ) {
		$breadcrumbs .= $sep . '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">Recipes</span></li>';
	}

    // Add Title of page
    if( is_page() ) {
        if( wp_get_post_parent_id( $post->ID ) ) {
            $parent_id = wp_get_post_parent_id( $post->ID );
            $parent_title = get_the_title( $parent_id );
            $parent_url = get_the_permalink( $parent_id );

            $breadcrumbs .= 
            $sep . 
            '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . $parent_url. '">' . $parent_title . '</a>'.
            $sep .
            '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">' . get_the_title( $post ) . '</span></li>';
        }
        else {
            $breadcrumbs .= $sep . '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">' . get_the_title( $post ) . '</span></li>';
        }
    }

	// Add blog page title then post page
	if( is_single() && !is_singular( 'recipes' ) ) {
        $blog_url = get_permalink( get_option( 'page_for_posts' ) );
        $blog_name = get_the_title( get_option( 'page_for_posts' ) );

        $breadcrumbs .= 
        $sep .
        '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . $blog_url . '">' . $blog_name . '</a>'.
        $sep .
        '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">' . get_the_title( $post ) . '</span></li>';
	}

	if( is_singular( 'recipes' ) ) {
		$archive_url = get_post_type_archive_link( 'recipes' );

		$breadcrumbs .= 
        $sep .
        '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . $archive_url . '">Recipes</></a>'.
        $sep .
        '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">' . get_the_title( $post ) . '</span></li>';
	}

	if( is_category() ) {
		$category_name = single_cat_title( "", false );

        $breadcrumbs .= 
        $sep .
        '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">'.$category_name.'</span></li>';
	}
	
	if( is_tag() ) {
		$tag_name = single_tag_title( "", false );

        $breadcrumbs .= 
        $sep .
        '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">'.$tag_name.'</span></li>';
    }

    if( is_404() ) {
        $breadcrumbs .= 
        $sep .
        '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item">404: Page not found</span></li>';
    }

    echo $before_wrap . $breadcrumbs . $after_wrap;
}

// RECENT POST WIDGET AS SHORTCODE
function recent_posts_shortcode( $atts ) {
    $a = shortcode_atts( array(
       'posts' => 4,
       'wrapper_class' => 'latest-news',
       'title' => 'Recent Posts',
    ), $atts );

    global $post;

    $r = new WP_Query( 
        apply_filters( 
            'widget_posts_args', 
            array( 
                'posts_per_page' => $a['posts'], 
                'no_found_rows' => true, 
                'post_status' => 'publish', 
                'ignore_sticky_posts' => true,
                'post__not_in'      => array( $post->ID ), // Exclude current post
            ) 
        ) 
    );
    $html = "";

    if( $r->have_posts() ) :
        $html .=    '<h2>'.$a['title'].'</h2>
                    <div class="'.$a['wrapper_class'].'">';

        while( $r->have_posts() ) : $r->the_post();
            $url = get_permalink();

            if ( has_post_thumbnail()):
                $thumb_url = '<figure style="background-image: url('.get_the_post_thumbnail_url( $post->ID, 'latest-news-thumb' ).');" class="latest-news__item--thumb"></figure>';
            endif;
            $time = get_the_time( 'M d');
            $author = get_the_author();
            $title = get_the_title();
            $content = wp_trim_words( get_the_content(), $num_words = 15, $more = '...' );

            $html .=    '<div class="latest-news__item">'
                            .'<a href="'.$url.'">'.( $thumb_url ? $thumb_url : NULL).'</a>
                            <article class="latest-news__item--content">
                                <header>
                                    <span>
                                        <time datetime="M-d">'.$time.'</time> by <span>'.$author.'</span>
                                    </span>

                                    <div class="news-likes">
                                        <img src="'.get_template_directory_uri().'/images/logos-and-icons/heart-off.png" alt="Like Icon">
                                        <span>22</span>
                                    </div>
            
                                    <div class="news-comments">
                                        <img src="'.get_template_directory_uri().'/images/logos-and-icons/noun_comment_847909.png" alt="Comment Icon">
                                        <span>'.get_comments_number().'</span>
                                    </div>
                                </header>

                                <a href="'.$url.'"><h3>'.$title.'</h3></a>

                                <p>'.$content.'</p>

                                <a href="'.$url.'" class="read-more">
                                    <span>Read more</span>
                                    <img src="'.get_template_directory_uri().'/images/logos-and-icons/arrow-right.png" alt="See more">
                                </a>
                            </article>
                        </div>';
        endwhile;
        $html .= '</div>';

    wp_reset_postdata();
    
    endif;
    
    return $html;
}
add_shortcode( 'recent_posts', 'recent_posts_shortcode' );