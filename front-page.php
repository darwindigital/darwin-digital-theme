<?php
/**
 * The default page displayed
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package darwin
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
<?php if ( have_posts() ) : ?>

    <?php
    // Start the loop.
    while ( have_posts() ) :
        the_post();

        // echo the_title();
        // echo the_content();
    
        /*
         * Include the Post-Format-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
         */
        get_template_part( 'content', get_post_format() );
        // End the loop.
    endwhile;
    // Previous/next page navigation.
    the_posts_pagination(
        array(
            'prev_text'          => __( 'Previous page', 'darwin' ),
            'next_text'          => __( 'Next page', 'darwin' ),
            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'darwin' ) . ' </span>',
        )
    );
else :
    // When no posts are found, output this text.                           
    _e( 'Sorry, no posts matched your criteria.' ); 
endif;
?>

</main> <!-- #main -->

<?php get_footer(); ?>