## Favicon ##
Generated and tested on https://realfavicongenerator.net/

Put the following in server root directory:
* mstile-150x150.png
* android-chrome-192x192.png
* android-chrome-256x256.png