��          �      �       0  #   1  N   U  E   �  	   �     �     �               $     ;  &   J  *   q  �  �  $   �  c   �  \        l     {     �  
   �     �     �     �  M   �  #   '     
                                  	                   A short description of the sidebar. Default canvas developed to make the Darwin Digital's websites creation easier It looks like nothing was found at this location. Maybe try a search? Next page Page Previous page Primary Menu Primary Sidebar Search Results for: %s Secondary Menu Sorry, no posts matched your criteria. This is somewhat embarrassing, isn’t it? Project-Id-Version: Darwin Digital WP Theme Pot v1.0.0
POT-Creation-Date: 2019-06-07 14:25+0200
PO-Revision-Date: 
Language-Team: Darwin Digital
Report-Msgid-Bugs-To: Translator Name <translations@example.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Textdomain-Support: yesX-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2
X-Poedit-Basepath: ..
X-Generator: Poedit 2.2.3
Last-Translator: 
Language: de
X-Poedit-SearchPath-0: .
 Eine kurze Beschreibung des sidebar. Unterlassungssegeltuch entwickelt, um die Website-Gestaltung von Darwin Digital einfacher zu machen Es sieht aus, dass nichts an diesem Ort gefunden wurde. Versuchen Sie vielleicht eine Suche? nächste Seite Seite vorhergehende Seite Hauptmenü Vorwahl sidebar Suchergebnisse für: %s Sekundäre Menü Es tut mir leid, aber es gibt keine Artikel, die Ihren Kriterien entsprechen. Das ist etwas peinlich, nicht wahr? 